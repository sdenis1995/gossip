package main

import (
	"math/rand"
	"strconv"
  "net"
  "time"
  "encoding/json"
  "sync"
  "fmt"
  "os"
)

type semaphore chan int
// acquire n resources
func (s semaphore) P(n int) {
    for i := 0; i < n; i++ {
        s <- 0
    }
}

// release n resources
func (s semaphore) V(n int) { 
    for i := 0; i < n; i++ {
        <-s
    }
}
var sem semaphore

func sendResponse(conn *net.UDPConn, addr *net.UDPAddr, message []byte) {
    _,err := conn.WriteToUDP(message, addr)
    if err != nil {
        fmt.Printf("Couldn't send response %v", err)
    }
}



type Message struct {
  Id int64
  Type_ string
  Sender int
  Origin int
  Data string
}

type Node struct {
	id   int
	port int
}

type Graph map[Node][]Node
//type conMap map[int]*net.UDPConn
type conMap map[int]*net.UDPAddr

//global variables
var basePort int
var delay time.Duration = time.Millisecond * 25
var globalMessageId int64 = 1
var idLock sync.Mutex
var notNotifiedNodes map[int]int
var wg, wg1 sync.WaitGroup
var dropProba float64 = 0

func newNode(a int) Node {
	return Node{
		id:   a,
		port: a + basePort,
	}
}

func (n Node) String() string {
	return strconv.Itoa(n.id)
}

func (n Node) Port() int {
	return n.port
}

func (g Graph) addEdge(a, b Node) {
	g[a] = append(g[a], b)
	g[b] = append(g[b], a)
}

func (g Graph) Neighbors(id int) ([]Node, bool) {
	node := newNode(id)
	nodes, ok := g[node]
	return nodes, ok
}

func (g Graph) GetNode(id int) (Node, bool) {
	node := newNode(id)
	_, ok := g[node]
	return node, ok
}

// Generate generates connected random graph with n nodes.
// The degree of most part of vertices will be in interval [minDegree:maxDegree]
// some (not many) vertices can have more than maxDegree vertices.
//
// Port value is used as a base value for a port
// each generated node i (0 <= 0 <= n) will be associated
// with port p (p = i + port)
//
// Generate uses standard golang rand package,
// it is user's responsibility to init rand with a propper seed.
func Generate(n, minDegree, maxDegree, port int) Graph {
	if minDegree <= 0 || minDegree > maxDegree {
		panic("wrong minDegree or maxDegree value")
	}

	basePort = port
	g := make(Graph)

	degrees := make(map[int]int, n)
	for i := 0; i < n; i++ {
		degrees[i] = rand.Intn(maxDegree-minDegree) + minDegree
	}

	perm := rand.Perm(n)
	for i := 1; i < n; i++ {
		a := perm[i-1]
		b := perm[i]
		g.addEdge(newNode(a), newNode(b))
		degrees[a]--
		degrees[b]--
		if degrees[a] <= 0 {
			delete(degrees, a)
		}
		if degrees[b] <= 0 {
			delete(degrees, b)
		}
	}

	var nodes []int
	for len(degrees) > 1 {
		nodes = nodes[:0]
		for node := range degrees {
			nodes = append(nodes, node)
		}

		cur, nodes := nodes[0], nodes[1:]
		perm := rand.Perm(len(nodes))

		max := degrees[cur]
		if max > len(perm) {
			max = len(perm)
		}
		a := newNode(cur)
		for index := range perm[:max] {
			neigh := nodes[index]
			degrees[neigh]--
			if degrees[neigh] <= 0 {
				delete(degrees, neigh)
			}
			b := newNode(neigh)
			g.addEdge(a, b)
		}
		delete(degrees, cur)
	}

	for v := range degrees {
		a := newNode(v)
		if len(g[a]) > 0 {
			continue
		}
		b := v
		for b == v {
			b = rand.Intn(n)
		}
		g.addEdge(a, newNode(b))
	}

	return g
}

func ClientOperate(me Node, neighbours []Node){
  rand.Seed(int64(int(time.Now().Unix()) * (me.id + 1)))
  wg.Done()
  wg.Wait()
  
  myId := me.id
  neighbourCons := make(conMap)
  
  sem.P(1)
  
  MyAddr,err := net.ResolveUDPAddr("udp","127.0.0.1:" + strconv.Itoa(me.port))
  if err != nil{
    panic("cant resolve my address")
  }
  
  ServerConn, err := net.ListenUDP("udp", MyAddr)
  if err != nil{
    panic("cant listen to udp")
  }
  
  sem.V(1)
  
  buf := make([]byte, 1024 * 500)
  for _, item := range neighbours{
    ServerAddress, err := net.ResolveUDPAddr("udp", "127.0.0.1:" + strconv.Itoa(item.port))
    if err != nil{
      panic("cant resolve address")
    }
    /*
    neighbourCons[item.id] = Conn
    */
    neighbourCons[item.id] = ServerAddress
  }
  
  start := time.Now()
  initialGossipMessages := make([]Message, 0)
  notificationPool := make([]Message, 0)
  if myId == 0 {
    tmpMessage := Message{
      0,
      "multicast",
      0,
      0,
      "start gossip muhaha"}
    
    initialGossipMessages = append(initialGossipMessages, tmpMessage)
  }
  // обработка принятых сообщений
  go func(){
    total_pckts_got := 0
    total_pckts_dropped := 0
    for {
      n,_,err := ServerConn.ReadFromUDP(buf)
      total_pckts_got++
      if err != nil{
        panic("cant read")
      }
      //simulate dropping the packets
      if rand.Float64() < dropProba{
        total_pckts_dropped++
        //fmt.Printf("id:%d, %f ratio\n", myId, float64(total_pckts_dropped)/float64(total_pckts_got))
        continue
      }
      tmpMessage := Message{}
      err_ := json.Unmarshal(buf[0:n], &tmpMessage)
      if err_ == nil {
        if tmpMessage.Type_ == "notification" {
          if myId == 0 {
            //вычеркиваем из общего списка узлов тот, уведомление которого получили
            //fmt.Printf("got notification from %d\n", tmpMessage.Origin)
            _, ok := notNotifiedNodes[tmpMessage.Origin]
            if ok {
              delete(notNotifiedNodes, tmpMessage.Origin)
              //fmt.Printf("%d left\n", len(notNotifiedNodes))
            }
            if len(notNotifiedNodes) == 0 {
              elapsed := time.Since(start)
              //fmt.Printf("Got all notifications in %s seconds\n", elapsed)
              fmt.Printf("%s\n", elapsed)
              os.Exit(0)
            }
          }else{
            flag := true
            
            for _, item := range notificationPool {
              if item.Origin == tmpMessage.Origin {
                flag = false
                break
              }
            }
            if flag {
              notificationPool = append(notificationPool, tmpMessage)
            }
          }
          //fmt.Printf("got notification on %d from %d: %s\n", myId, tmpMessage.Sender, buf[0:n])
        }
        if tmpMessage.Type_ == "multicast" {
          if myId != 0 {
            flag := true
            for _, item := range initialGossipMessages {
              if tmpMessage.Data == item.Data {
                flag = false
                break
              }
            }
            
            if flag{
              initialGossipMessages = append(initialGossipMessages, tmpMessage)
              //добавляем еще сообщение в notificationPool так как мы получили новый multicast
              tmpNotificationMessage := Message{}
              tmpNotificationMessage.Origin = myId
              tmpNotificationMessage.Sender = myId
              tmpNotificationMessage.Type_ = "notification"
              tmpNotificationMessage.Data = ""
              notificationPool = append(notificationPool, tmpNotificationMessage)
            }
            //fmt.Printf("got multicast on %d from %d: %s\n", myId, tmpMessage.Sender, buf[0:n])
          }
          
        }
      }
      
    }
  }()
  // реализация отправления периодически различных сообщений
  go func(){
    for {
      time.Sleep(delay)
      //обработка multicast сообщений
      for _, item := range initialGossipMessages{
        tmpGossipMessage := Message(item)
        tmpGossipMessage.Sender = myId
        for tmpId, con := range neighbourCons{
          if tmpId != item.Sender{
            idLock.Lock()
            globalMessageId++
            tmpGossipMessage.Id = globalMessageId
            idLock.Unlock()
            b, _ := json.Marshal(tmpGossipMessage)
            sendResponse(ServerConn, con, b)
            //con.Write(b)
            //fmt.Printf("%d sends to %d: %s\n", myId, tmpId, b)
          }
        }
      }
      //обработка notification сообщений
      for _, item := range notificationPool{
        tmpNotificationMessage := Message(item)
        tmpNotificationMessage.Sender = myId
        for tmpId, con := range neighbourCons{
          if tmpId != item.Sender {
            idLock.Lock()
            globalMessageId++
            tmpNotificationMessage.Id = globalMessageId
            idLock.Unlock()
            b, _ := json.Marshal(tmpNotificationMessage)
            sendResponse(ServerConn, con, b)
            //con.Write(b)
            //fmt.Printf("%d sends to %d: %s\n", myId, tmpId, b)
          }
        }
      }
      
    }
  }()
  
}

func main(){
  rand.Seed(10);
  if len(os.Args) > 1{
    dropProba_tmp, err := strconv.Atoi(os.Args[1])
    if err != nil{
      panic(err)
      dropProba = 0
    }else{
      dropProba = float64(dropProba_tmp) / 10.0
    }
      
  }
  //fmt.Printf("%f\n", dropProba)
	config_graph := Generate(1000, 3, 10, 30000)
  notNotifiedNodes = make(map[int]int)
  //initialize notNotifiedNodes
  for k, _ := range config_graph{
    if k.id != 0 {
      notNotifiedNodes[k.id] = 1
    }
  }
  count := len(config_graph)
  sem = make(semaphore, count)
  wg.Add(count)
  wg1.Add(count)
  
  for k, _ := range config_graph{
    neighbours, ok := config_graph.Neighbors(k.id)
    /*
    for _, l := range neighbours{
      fmt.Printf("%d -> %d\n", k.id, l.id)
    }
    */
    if ok{
      go ClientOperate(k, neighbours)
    }else{
      panic("Error : no neighbours of a node")
    }
  }
  wg1.Wait()
}
